﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContpaqWL;

namespace ContpaqSDK
{
    /// <summary>
    /// Libreria de alto nivel para el manejo del SDK de Contpaq.
    /// </summary>
    public class ContpaqNET : IDisposable
    {
        Contpaq csdk;
        public ContpaqNET()
        {
            csdk = new Contpaq();
            csdk.InicializaSDK();
        }

        ~ContpaqNET()
        {
            this.Dispose(false);
        }

        #region Estado Interno

        private Empresa _empresaAbierta;
        private bool _hayEmpresaAbierta = false;

        #endregion

        #region Funciones privadas

        private void LanzarErrorSiExiste(int numError)
        {
            if (numError == 0) return;
            string mensaje = "";
            mensaje = csdk.DescripcionError(numError);
            throw new ContpaqException(mensaje, numError);
        }

        #endregion

        #region Funciones Generales

        public string Error(int numError)
        {
            return csdk.DescripcionError(numError);
        }

        public void AbreEmpresa(Empresa Empresa)
        {
            int error = csdk.AbreEmpresa(Empresa.Directorio);
            LanzarErrorSiExiste(error);
            _hayEmpresaAbierta = true;
            _empresaAbierta = Empresa;
        }

        public void CierraEmpresa()
        {
            csdk.CierraEmpresa();
            _hayEmpresaAbierta = false;
            _empresaAbierta = new Empresa();
        }

        public List<Empresa> EnlistarEmpresas()
        {
            List<Empresa> ret = new List<Empresa>();
            Empresa tmp = new Empresa();
            int error = 0;

            error = csdk.PosPrimerEmpresa(ref tmp.IdEmpresa, ref tmp.Nombre, ref tmp.Directorio);
            LanzarErrorSiExiste(error);
            ret.Add(tmp);

            while (error == 0)
            {
                tmp = new Empresa();
                error = csdk.PosSiguienteEmpresa(ref tmp.IdEmpresa, ref tmp.Nombre, ref tmp.Directorio);
                ret.Add(tmp);
            };
            return ret;
        }

        #endregion

        #region Funciones de Documentos 
        public int AltaDocumento(Documento Documento)
        {
            /* TODO: Probar el funcionamiento de esta funcion */
            int idDoc = 0;
            csdk.AltaDocumento(ref idDoc, Documento);
            return idDoc;
        }
        #endregion

        #region Funciones de Clientes/Proveedores 

        public int AltaCteProv(ClienteProveedor CteProv)
        {
            int idCteProv = 0;
            LanzarErrorSiExiste(csdk.AltaCteProv(ref idCteProv, CteProv));
            return idCteProv;
        }
        #endregion

        #region IDisposable Members

        bool _disposed = false;

        private void Dispose(bool Disposing)
        {
            if (_disposed) throw new ObjectDisposedException("ContpaqNET");
            if (Disposing)
            {
                //Librerar aqui recursos administrados
            }
            if (csdk != null) csdk.TerminaSDK();
            _disposed = true;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this); //Evita doble llamada a Dispose(bool)
        }

        #endregion
    }
}
