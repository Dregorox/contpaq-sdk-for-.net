#include "stdafx.h"
#include "CommonFunctions.h"
#include "Managed.h"

#pragma region RegDocumento


/* Copia los datos de la estructura de .Net Documento a una estructura para poder utilizar en el SDK de Contpaq */
LPFREGDOCUMENTO ContpaqWL::Documento::ToCStruct (){
	LPFREGDOCUMENTO doc = (LPFREGDOCUMENTO)( new REGDOCUMENTO() );
	doc->aFolio = this->Folio;
	doc->aNumMoneda = this->NumMoneda;
	doc->aTipoCambio = this->TipoCambio;
	doc->aImporte = this->Importe;
	doc->aDescuentoDoc1 = this->DescuentoDoc1;
	doc->aDescuentoDoc2 = this->DescuentoDoc2;
	doc->aSistemaOrigen = this->SistemaOrigen;
	strncpy_s (doc->aCodConcepto,ToAnsiString(this->CodConcepto),  kLongCodigo);
	strncpy_s (doc->aSerie,ToAnsiString(this->Serie),  kLongSerie);
	strncpy_s (doc->aFecha,ToAnsiString(this->Fecha),  kLongFecha);
	strncpy_s (doc->aCodigoCteProv,ToAnsiString(this->CodigoCteProv),  kLongCodigo);
	strncpy_s (doc->aCodigoAgente,ToAnsiString(this->CodigoAgente),  kLongCodigo);
	strncpy_s (doc->aReferencia,ToAnsiString(this->Referencia),  kLongReferencia);
	doc->aAfecta = this->Afecta;
	doc->aGasto1 = this->Gasto1;
	doc->aGasto2 = this->Gasto2;
	doc->aGasto3 = this->Gasto3;
	return doc;
}

#pragma endregion

/* Copia los datoa de la estructura de .Net Movimiento a una estructura para poder utilizar en el SDK de Contpaq */
LPFREGMOVIMIENTO ContpaqWL::Movimiento::ToCStruct(){
	LPFREGMOVIMIENTO mov = (LPFREGMOVIMIENTO)(new REGMOVIMIENTO());
	mov->aConsecutivo = this->Consecutivo;
	mov->aUnidades = this->Unidades;
	mov->aPrecio = this->Precio;
	mov->aCosto = this->Costo;
	strncpy_s (mov->aCodProdSer,ToAnsiString(this->CodProdSer),  kLongCodigo);
	strncpy_s (mov->aCodAlmacen,ToAnsiString(this->CodAlmacen),  kLongCodigo);
	strncpy_s (mov->aReferencia,ToAnsiString(this->Referencia),  kLongReferencia);
	strncpy_s (mov->aCodClasificacion,ToAnsiString(this->CodClasificacion),  kLongCodigo);
	return mov;
}

LPFREGCTEPROV ContpaqWL::ClienteProveedor::ToCStruct(){
	LPFREGCTEPROV cp = (LPFREGCTEPROV)(new LPFREGCTEPROV());

	strncpy_s(cp->cCodigoCliente, String::IsNullOrEmpty( this->CodigoCliente)?"":ToAnsiString(this->CodigoCliente), kLongCodigo);
	strncpy_s(cp->cRazonSocial, String::IsNullOrEmpty(this->RazonSocial)?"":ToAnsiString(this->RazonSocial), kLongNombre);
	strncpy_s(cp->cFechaAlta, (this->FechaAlta == nullptr)?"":ToAnsiString(this->FechaAlta->ToString("MM/dd/yyyy")), kLongFecha);
	strncpy_s(cp->cRFC, String::IsNullOrEmpty(this->RFC)?"":ToAnsiString(this->RFC), kLongRFC);
	strncpy_s(cp->cCURP, String::IsNullOrEmpty(this->CURP)?"":ToAnsiString(this->CURP), kLongCURP);
	strncpy_s(cp->cDenComercial, String::IsNullOrEmpty(this->DenComercial)?"":ToAnsiString(this->DenComercial), kLongDenComercial);
	strncpy_s(cp->cRepLegal, String::IsNullOrEmpty(this->RepLegal)?"":ToAnsiString(this->RepLegal), kLongRepLegal);
	strncpy_s(cp->cNombreMoneda, String::IsNullOrEmpty(this->NombreMoneda)?"":ToAnsiString(this->NombreMoneda), kLongNombre);
	cp->cListaPreciosCliente = this->ListaPreciosCliente;
	cp->cDescuentoMovto = this->DescuentoMovto;
	cp->cBanVentaCredito = this->BanVentaCredito;
	strncpy_s(cp->cCodigoValorClasificacionCliente1, String::IsNullOrEmpty(this->CodigoValorClasificacionCliente1)?"":ToAnsiString(this->CodigoValorClasificacionCliente1), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionCliente2, String::IsNullOrEmpty(this->CodigoValorClasificacionCliente2)?"":ToAnsiString(this->CodigoValorClasificacionCliente2), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionCliente3, String::IsNullOrEmpty(this->CodigoValorClasificacionCliente3)?"":ToAnsiString(this->CodigoValorClasificacionCliente3), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionCliente4, String::IsNullOrEmpty(this->CodigoValorClasificacionCliente4)?"":ToAnsiString(this->CodigoValorClasificacionCliente4), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionCliente5, String::IsNullOrEmpty(this->CodigoValorClasificacionCliente5)?"":ToAnsiString(this->CodigoValorClasificacionCliente5), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionCliente6, String::IsNullOrEmpty(this->CodigoValorClasificacionCliente6)?"":ToAnsiString(this->CodigoValorClasificacionCliente6), kLongCodValorClasif);
	cp->cTipoCliente = this->TipoCliente;
	cp->cEstatus = this->Estatus;
	/*Muy probablemente estas fechas nisiquiera sean necesrias para la alta del cliente/proveedor */
	strncpy_s(cp->cFechaBaja, (this->FechaBaja == nullptr)?"":ToAnsiString(this->FechaBaja->ToString("MM/dd/yyyy")), kLongFecha);
	strncpy_s(cp->cFechaUltimaRevision, (this->FechaUltimaRevision == nullptr)?"":ToAnsiString(this->FechaUltimaRevision->ToString("MM/dd/yyyy")), kLongFecha);
	cp->cLimiteCreditoCliente = this->LimiteCreditoCliente;
	cp->cDiasCreditoCliente = this->DiasCreditoCliente;
	cp->cBanExcederCredito = this->BanExcederCredito;
	cp->cDescuentoProntoPago = this->DescuentoProntoPago;
	cp->cDiasProntoPago = this->DiasProntoPago;
	cp->cInteresMoratorio = this->InteresMoratorio;
	cp->cDiaPago = this->DiaPago;
	cp->cDiasRevision = this->DiasRevision;
	strncpy_s(cp->cMensajeria, String::IsNullOrEmpty(this->Mensajeria)?"":ToAnsiString(this->Mensajeria), kLongDesCorta);
	strncpy_s(cp->cCuentaMensajeria, String::IsNullOrEmpty(this->CuentaMensajeria)?"":ToAnsiString(this->CuentaMensajeria), kLongDescripcion);
	cp->cDiasEmbarqueCliente = this->DiasEmbarqueCliente;
	strncpy_s(cp->cCodigoAlmacen, String::IsNullOrEmpty(this->CodigoAlmacen)?"":ToAnsiString(this->CodigoAlmacen), kLongCodigo);
	strncpy_s(cp->cCodigoAgenteVenta, String::IsNullOrEmpty(this->CodigoAgenteVenta)?"":ToAnsiString(this->CodigoAgenteVenta), kLongCodigo);
	strncpy_s(cp->cCodigoAgenteCobro, String::IsNullOrEmpty(this->CodigoAgenteCobro)?"":ToAnsiString(this->CodigoAgenteCobro), kLongCodigo);
	cp->cRestriccionAgente = this->RestriccionAgente;
	cp->cImpuesto1 = this->Impuesto1;
	cp->cImpuesto2 = this->Impuesto2;
	cp->cImpuesto3 = this->Impuesto3;
	cp->cRetencionCliente1 = this->RetencionCliente1;
	cp->cRetencionCliente2 = this->RetencionCliente2;
	strncpy_s(cp->cCodigoValorClasificacionProveedor1 , String::IsNullOrEmpty(this->CodigoValorClasificacionProveedor1)?"":
		ToAnsiString(this->CodigoValorClasificacionProveedor1), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionProveedor2 , String::IsNullOrEmpty(this->CodigoValorClasificacionProveedor2)?"":
		ToAnsiString(this->CodigoValorClasificacionProveedor2), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionProveedor3 , String::IsNullOrEmpty(this->CodigoValorClasificacionProveedor3)?"":
		ToAnsiString(this->CodigoValorClasificacionProveedor3), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionProveedor4 , String::IsNullOrEmpty(this->CodigoValorClasificacionProveedor4)?"":
		ToAnsiString(this->CodigoValorClasificacionProveedor4), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionProveedor5 , String::IsNullOrEmpty(this->CodigoValorClasificacionProveedor5)?"":
		ToAnsiString(this->CodigoValorClasificacionProveedor5), kLongCodValorClasif);
	strncpy_s(cp->cCodigoValorClasificacionProveedor6 , String::IsNullOrEmpty(this->CodigoValorClasificacionProveedor6)?"":
		ToAnsiString(this->CodigoValorClasificacionProveedor6), kLongCodValorClasif);
	cp->cLimiteCreditoProveedor = this->LimiteCreditoProveedor;
	cp->cDiasCreditoProveedor = this->DiasCreditoProveedor;
	cp->cTiempoEntrega = this->TiempoEntrega;
	cp->cDiasEmbarqueProveedor = this->DiasEmbarqueProveedor;
	cp->cImpuestoProveedor1 = this->ImpuestoProveedor1;
	cp->cImpuestoProveedor2 = this->ImpuestoProveedor2;
	cp->cImpuestoProveedor3 = this->ImpuestoProveedor3;
	cp->cRetencionProveedor1 = this->RetencionProveedor1;
	cp->cRetencionProveedor2 = this->RetencionProveedor2;
	cp->cBanInteresMoratorio = this->BanInteresMoratorio;
	strncpy_s(cp->cTextoExtra1, String::IsNullOrEmpty(this->TextoExtra1)?"":ToAnsiString(this->TextoExtra1), kLongTextoExtra);
	strncpy_s(cp->cTextoExtra2, String::IsNullOrEmpty(this->TextoExtra2)?"":ToAnsiString(this->TextoExtra2), kLongTextoExtra);
	strncpy_s(cp->cTextoExtra3, String::IsNullOrEmpty(this->TextoExtra3)?"":ToAnsiString(this->TextoExtra3), kLongTextoExtra);
	strncpy_s(cp->cFechaExtra, (this->FechaExtra == nullptr)?"":ToAnsiString(this->FechaExtra->ToString("MM/dd/yyyy")), kLongFecha);
	cp->cImporteExtra1 = this->ImporteExtra1;
	cp->cImporteExtra2 = this->ImporteExtra2;
	cp->cImporteExtra3 = this->ImporteExtra3;
	cp->cImporteExtra4 = this->ImporteExtra4;
	return cp;
}

