// This is the main DLL file.

#include "Stdafx.h"
#include "ContpaqWL.h"
#include "CommonFunctions.h"

using namespace ContpaqWL;
using namespace System;
using namespace std;
using namespace System::Runtime::InteropServices;

/*
Constructor de la clase Core, se encarga de cargar la libreria del SDK, 
ademas de inicializar los apuntadores a la funciones. 


param libraryDirectory: Es el directorio a la biblioteca del SDK, o bien el directorio a la 
carpeta de instalacion del AdminPAQ SDK.
*/
Contpaq::Contpaq() : inicializado(false){
	SetErrorMode (0); //Muestra todos los mensajes de error durante la carga.
	string s("MGW_SDK.dll");
	wstring directory = s2ws(s);
	hDll = LoadLibrary((LPCWSTR)directory.c_str());
	if (hDll == NULL){
		DWORD lastError = GetLastError(); 
		int lError = lastError;
		throw gcnew System::ApplicationException(String::Format("No se pudo cargar el SDK de AdminPAQ [{0}]" , lError.ToString()));
	}
	/* Funciones generales DSK */
	fInicializaSDK = (LPFNDLL_IV)GetProcAddress(hDll, "fInicializaSDK");
	fTerminaSDK = (LPFNDLL_VV)GetProcAddress(hDll, "fTerminaSDK");
	fError = (LPFNDLL_VICI)GetProcAddress(hDll, "fError");
	fPosPrimerEmpresa = (LPFNDLL_IICC)GetProcAddress(hDll, "fPosPrimerEmpresa");
	fPosSiguienteEmpresa = (LPFNDLL_IICC)GetProcAddress(hDll, "fPosSiguienteEmpresa");
	fAbreEmpresa  = (LPFNDLL_IC)GetProcAddress(hDll, "fAbreEmpresa");
	fCierraEmpresa = (LPFNDLL_VV)GetProcAddress(hDll, "fCierraEmpresa");
	// Documentos/Movimientos
	fAltaDocumento = (LPFNDLL_ALTADOCUMENTO)GetProcAddress(hDll, "fAltaDocumento");
	fAltaMovimiento = (LPFNDLL_ALTAMOVIMIENTO)GetProcAddress(hDll, "fAltaMovimiento");
	// Clientes/Proveedores
	fBuscaIdCteProv = (LPFNDLL_II)GetProcAddress(hDll, "fBuscaIdCteProv");
	fBuscaCteProv = (LPFNDLL_IC)GetProcAddress(hDll, "fBuscaCteProv");

	fLeeDatoCteProv = (LPFNDLL_LEEDATOCTEPROV)GetProcAddress(hDll, "fLeeDatoCteProv");
	fAltaCteProv = (LPFNDLL_ALTACTEPROV)GetProcAddress(hDll, "fAltaCteProv");
	fActualizaCteProv = (LPFNDLL_ACTUALIZACTEPROV)GetProcAddress(hDll, "fActualizaCteProv");

	fPosPrimerCteProv = (LPFNDLL_IV)GetProcAddress(hDll, "fPosPrimerCteProv");
	fPosUltimoCteProv = (LPFNDLL_IV)GetProcAddress(hDll, "fPosUltimoCteProv");
	fPosSiguienteCteProv = (LPFNDLL_IV)GetProcAddress(hDll, "fPosSiguienteCteProv");
	fPosAnteriorCteProv = (LPFNDLL_IV)GetProcAddress(hDll, "fPosAnteriorCteProv");
	fPosBOFCteProv = (LPFNDLL_IV)GetProcAddress(hDll, "fPosBOFCteProv");
	fPosEOFCteProv = (LPFNDLL_IV)GetProcAddress(hDll, "fPosEOFCteProv");
		
};

void Contpaq::InicializaSDK( void ) {
	/* Codigo de inicializacion. */
	if (fInicializaSDK == NULL) 
		throw gcnew System::ApplicationException("La funcion inicializar es nula, no se puede continuar");
	int error = this->fInicializaSDK();
	inicializado = true;
}

void Contpaq::TerminaSDK ( void ) {
	this->fTerminaSDK();
};

String^ Contpaq::DescripcionError(int aNumError) {
	char message[MAXSTRINGLEN];
	this->fError (aNumError, message, MAXSTRINGLEN);
	/* Cast char* to String^ */
	return  gcnew String(message);
};

int Contpaq::PosPrimerEmpresa (int%  IdEmpresa, String^% NombreEmpresa, String^% DirectorioEmpresa){
	int id ;
	char nombre[MAXSTRINGLEN];
	char directorio [MAXSTRINGLEN];
	int error;

	error = this->fPosPrimerEmpresa(id, nombre, directorio);
	IdEmpresa = id;
	NombreEmpresa = gcnew String(nombre);
	DirectorioEmpresa = gcnew String(directorio);
	return error;
}

int Contpaq::PosSiguienteEmpresa (int% IdEmpresa, String^% NombreEmpresa, String^% DirectorioEmpresa){
	int id ;
	char nombre[MAXSTRINGLEN];
	char directorio [MAXSTRINGLEN];
	int error;

	error = this->fPosSiguienteEmpresa(id, nombre, directorio);
	IdEmpresa = id;
	NombreEmpresa = gcnew String(nombre);
	DirectorioEmpresa = gcnew String(directorio);
	return error;
}

int Contpaq::AbreEmpresa (String^ DirectorioEmpresa){
	char* directorio = (char*)(void*)Marshal::StringToHGlobalAnsi(DirectorioEmpresa);
	int error = this->fAbreEmpresa (directorio);
	Marshal::FreeHGlobal((System::IntPtr) directorio); //Libreamos la cadena.
	return error;
}
void Contpaq::CierraEmpresa ( void ){
	this->fCierraEmpresa();
}

#pragma  region Funciones de Documentos Movimientos - ALto Nivel 

int Contpaq::AltaDocumento ( long% IdDocumento, ContpaqWL::Documento Documento){
	long idDoc = IdDocumento;
	int error = fAltaDocumento (idDoc, Documento.ToCStruct());
	IdDocumento = idDoc;
	return error;
}

int Contpaq::AltaMovimiento (long IdDocumento, long% IdMovimiento, ContpaqWL::Movimiento Movimiento){
	int error; 
	long idMovimiento = 0;
	error = fAltaMovimiento (IdDocumento, idMovimiento, Movimiento.ToCStruct());
	IdMovimiento = idMovimiento;
	return error;
}
#pragma endregion

#pragma region Clientes Proveedores - Alto Nivel

int Contpaq::LeeDatoCteProv (String^ Campo, String^% Valor){
	int error;
	char* campo = ToAnsiString(Campo);
	char valor[kMaxFieldLen + 1] = "\0";

	error = fLeeDatoCteProv(campo, valor, kMaxFieldLen);
	Valor = gcnew String(valor);
	return error;
}
int Contpaq::AltaCteProv (int% IdCteProv, ContpaqWL::ClienteProveedor CteProv){
	int error;
	int idCteProv = 0;
	LPFREGCTEPROV cteProv = CteProv.ToCStruct();
	error = fAltaCteProv (idCteProv, cteProv);
	IdCteProv = idCteProv;
	return error;
}
int Contpaq::ActualizaCteProv (String^ CodigoCteProv, ContpaqWL::ClienteProveedor CteProv){
	int error;
	char* codCteProv = ToAnsiString(CodigoCteProv);
	error = fActualizaCteProv (codCteProv, CteProv.ToCStruct());
	return error;
}

#pragma endregion

#pragma region Busqueda y Navegacion - Bajo Nivel

int Contpaq::BuscaIdCteProv (int IdCteProv){
	return fBuscaIdCteProv (IdCteProv);
}
int Contpaq::BuscaCteProv(String^ CodCteProv){
	char* cod = ToAnsiString(CodCteProv);
	return fBuscaCteProv(cod);
}
int Contpaq::PosPrimerCteProv(){
	return fPosPrimerCteProv();
}
int Contpaq::PosUltimoCteProv(){
	return fPosUltimoCteProv();
}
int Contpaq::PosSiguienteCteProv(){
	return fPosSiguienteCteProv();
}
int Contpaq::PosAnteriorCteProv(){
	return fPosAnteriorCteProv();
}
int Contpaq::PosBOFCteProv(){
	return fPosBOFCteProv();
}
int Contpaq::PosEOFCteProv(){
	return fPosEOFCteProv();
}
#pragma endregion
/*****
ContpaqException Class
**/
ContpaqException::ContpaqException (String^ Message, int ErrorNumber) : 
	System::ApplicationException(String::Format("{0} ({1})", Message, ErrorNumber)){

}
