#ifndef MGW_SDKH
#define MGW_SDKH

/***************************************************************************************************
Estructura de para grabar Documentos con un movimiento  (Cargos y Abonos)
***************************************************************************************************/
#pragma pack(push)
#pragma pack(4)
// Definici�n de estructura de datos de documentos -------------------------------------------------
typedef struct RegDocumento {
   double aFolio;
   int    aNumMoneda;
   double aTipoCambio;
   double aImporte;
   double aDescuentoDoc1;
   double aDescuentoDoc2;
   int    aSistemaOrigen;
   char   aCodConcepto[ kLongCodigo + 1 ];
   char   aSerie[ kLongSerie + 1 ];
   char   aFecha[ kLongFecha + 1 ];
   char   aCodigoCteProv[ kLongCodigo + 1 ];
   char   aCodigoAgente[ kLongCodigo + 1 ];
   char   aReferencia[ kLongReferencia + 1 ];
   int    aAfecta;
   double aGasto1;
   double aGasto2;
   double aGasto3;
} REGDOCUMENTO;

typedef REGDOCUMENTO FAR * LPFREGDOCUMENTO;

// Definici�n de estructura de datos de movimientos ------------------------------------------------
typedef struct RegMovimiento {
  int     aConsecutivo;
  double  aUnidades;
  double  aPrecio;
  double  aCosto;
  char    aCodProdSer[ kLongCodigo + 1 ];
  char    aCodAlmacen[ kLongCodigo + 1 ];
  char    aReferencia[ kLongReferencia + 1 ];
  char    aCodClasificacion[ kLongCodigo + 1 ];
} REGMOVIMIENTO;

typedef REGMOVIMIENTO FAR * LPFREGMOVIMIENTO;

// Definici�n de estructura de datos de movimientos con Descuentos ---------------------------------
typedef struct RegMovimientoCDesctos {
  int     aConsecutivo;
  double  aUnidades;
  double  aPrecio;
  double  aCosto;
  double  aPorcDescto1;
  double aImporteDescto1;
  double  aPorcDescto2;
  double aImporteDescto2;
  double  aPorcDescto3;
  double aImporteDescto3;
  double  aPorcDescto4;
  double aImporteDescto4;
  double  aPorcDescto5;
  double aImporteDescto5;
  char    aCodProdSer[ kLongCodigo + 1 ];
  char    aCodAlmacen[ kLongCodigo + 1 ];
  char    aReferencia[ kLongReferencia + 1 ];
  char    aCodClasificacion[ kLongCodigo + 1 ];
} REGMOVIMIENTOCDESCTOS;

typedef REGMOVIMIENTOCDESCTOS FAR * LPFREGMOVIMIENTOCDESCTOS;

// Definici�n de estructura de datos del la llave de documentos ------------------------------------
typedef struct RegLlaveDoc {
  char   aCodConcepto[ kLongCodigo + 1 ];
  char   aSerie[ kLongSerie + 1 ];
  double aFolio;
} REGLLAVEDOC;

typedef REGLLAVEDOC FAR * LPFREGLLAVEDOC;

// Definici�n de estructura de datos movimientos de Serie/Capas ------------------------------------
typedef struct SeriesCapas {
  double aUnidades;
  double aTipoCambio;
  char   aSeries[ kLongCodigo + 1 ];
  char   aPedimento[ kLongDescripcion + 1 ];
  char   aAgencia[ kLongDescripcion + 1 ];
  char   aFechaPedimento[ kLongFecha + 1 ];
  char   aNumeroLote[ kLongDescripcion + 1 ];
  char   aFechaFabricacion[ kLongFecha + 1 ];
  char   aFechaCaducidad[ kLongFecha + 1 ];
} SERIESCAPAS;

typedef SERIESCAPAS FAR * LPSERIESCAPAS;

// Definici�n de estructura de datos de caracteristicas --------------------------------------------
typedef struct Caracteristicas {
  double aUnidades;
  char   aValorCaracteristica1[ kLongDescripcion + 1 ];
  char   aValorCaracteristica2[ kLongDescripcion + 1 ];
  char   aValorCaracteristica3[ kLongDescripcion + 1 ];
} CARACTERISTICAS;

typedef CARACTERISTICAS FAR * LPCARACTERISTICAS;

// Definicion de estructura de datos de caracteristicas con unidades de peso y medida --------------
typedef struct CaracteristicasUnidades
{
  char   aUnidad[ kLongAbreviatura + 1 ];
  double aUnidades;
  double aUnidadesNC;
  char   aValorCaracteristica1[ kLongDescripcion + 1 ];
  char   aValorCaracteristica2[ kLongDescripcion + 1 ];
  char   aValorCaracteristica3[ kLongDescripcion + 1 ];
} CARACTERISTICASUNIDADES;

typedef CARACTERISTICASUNIDADES FAR * LPCARACTERISTICASUNIDADES;

// Definici�n de estructura de datos del tipo de producto  -----------------------------------------
typedef union TipoProducto {
  SeriesCapas     aSeriesCapas;
  Caracteristicas aCaracteristicas;
} TIPOPRODUCTO;

typedef TIPOPRODUCTO FAR * LPFTIPOPRODUCTO;

// Definici�n de estructura de datos del la llave de aperturas -------------------------------------
typedef struct RegLlaveAper {
  char aCodCaja[ kLongCodigo + 1 ];
  char aFechaApe[ kLongFecha + 1 ];
} REGLLAVEAPER;

typedef REGLLAVEAPER FAR * LPFREGLLAVEAPER;

// Definici�n de estructura de datos de productos --------------------------------------------------
typedef struct Producto
{
  char   cCodigoProducto[ kLongCodigo + 1 ];
  char   cNombreProducto[ kLongNombre + 1 ];
  char   cDescripcionProducto[ kLongNombreProducto + 1 ];
  int    cTipoProducto; // 1 = Producto, 2 = Paquete, 3 = Servicio
  char   cFechaAltaProducto[ kLongFecha + 1 ];
  char   cFechaBaja[ kLongFecha + 1 ];
  int    cStatusProducto; // 0 - Baja L�gica, 1 - Alta
  int    cControlExistencia; //
  int    cMetodoCosteo; // 1 = Costo Promedio en Base a Entradas, 2 = Costo Promedio en Base a Entradas Almacen, 3 = �ltimo costo, 4 = UEPS, 5 = PEPS, 6 = Costo espec�fico, 7 = Costo Estandar
  char   cCodigoUnidadBase[ kLongCodigo + 1 ];
  char   cCodigoUnidadNoConvertible[ kLongCodigo + 1 ];
  double cPrecio1;
  double cPrecio2;
  double cPrecio3;
  double cPrecio4;
  double cPrecio5;
  double cPrecio6;
  double cPrecio7;
  double cPrecio8;
  double cPrecio9;
  double cPrecio10;
  double cImpuesto1;
  double cImpuesto2;
  double cImpuesto3;
  double cRetencion1;
  double cRetencion2;
  char   cNombreCaracteristica1[ kLongAbreviatura + 1 ];
  char   cNombreCaracteristica2[ kLongAbreviatura + 1 ];
  char   cNombreCaracteristica3[ kLongAbreviatura + 1 ];
  char   cCodigoValorClasificacion1[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacion2[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacion3[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacion4[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacion5[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacion6[ kLongCodValorClasif + 1 ];
  char   cTextoExtra1[ kLongTextoExtra + 1 ];
  char   cTextoExtra2[ kLongTextoExtra + 1 ];
  char   cTextoExtra3[ kLongTextoExtra + 1 ];
  char   cFechaExtra[ kLongFecha + 1 ];
  double cImporteExtra1;
  double cImporteExtra2;
  double cImporteExtra3;
  double cImporteExtra4;
} PRODUCTO;

typedef PRODUCTO FAR * LPFREGPRODUCTO;

// Definici�n de estructura de datos de cliente/proveedor ------------------------------------------
typedef struct ClienteProveedor
{
  char   cCodigoCliente[ kLongCodigo + 1 ];
  char   cRazonSocial[ kLongNombre + 1 ];
  char   cFechaAlta[ kLongFecha + 1 ];
  char   cRFC[ kLongRFC + 1 ];
  char   cCURP[ kLongCURP + 1 ];
  char   cDenComercial[ kLongDenComercial + 1 ];
  char   cRepLegal[ kLongRepLegal + 1 ];
  char   cNombreMoneda[ kLongNombre + 1 ];
  int    cListaPreciosCliente;
  double cDescuentoMovto;
  int    cBanVentaCredito; // 0 = No se permite venta a cr�dito, 1 = Se permite venta a cr�dito
  char   cCodigoValorClasificacionCliente1[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionCliente2[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionCliente3[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionCliente4[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionCliente5[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionCliente6[ kLongCodValorClasif + 1 ];
  int    cTipoCliente; // 1 - Cliente, 2 - Cliente/Proveedor, 3 - Proveedor
  int    cEstatus; // 0. Inactivo, 1. Activo
  char   cFechaBaja[ kLongFecha + 1 ];
  char   cFechaUltimaRevision[ kLongFecha + 1 ];
  double cLimiteCreditoCliente;
  int    cDiasCreditoCliente;
  int    cBanExcederCredito; // 0 = No se permite exceder cr�dito, 1 = Se permite exceder el cr�dito
  double cDescuentoProntoPago;
  int    cDiasProntoPago;
  double cInteresMoratorio;
  int    cDiaPago;
  int    cDiasRevision;
  char   cMensajeria[ kLongDesCorta + 1 ];
  char   cCuentaMensajeria[ kLongDescripcion + 1 ];
  int    cDiasEmbarqueCliente;
  char   cCodigoAlmacen[ kLongCodigo + 1 ];
  char   cCodigoAgenteVenta[ kLongCodigo + 1 ];
  char   cCodigoAgenteCobro[ kLongCodigo + 1 ];
  int    cRestriccionAgente;
  double cImpuesto1;
  double cImpuesto2;
  double cImpuesto3;
  double cRetencionCliente1;
  double cRetencionCliente2;
  char   cCodigoValorClasificacionProveedor1[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionProveedor2[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionProveedor3[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionProveedor4[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionProveedor5[ kLongCodValorClasif + 1 ];
  char   cCodigoValorClasificacionProveedor6[ kLongCodValorClasif + 1 ];
  double cLimiteCreditoProveedor;
  int    cDiasCreditoProveedor;
  int    cTiempoEntrega;
  int    cDiasEmbarqueProveedor;
  double cImpuestoProveedor1;
  double cImpuestoProveedor2;
  double cImpuestoProveedor3;
  double cRetencionProveedor1;
  double cRetencionProveedor2;
  int    cBanInteresMoratorio; // 0 = No se le calculan intereses moratorios al cliente, 1 = Si se le calculan intereses moratorios al cliente.
  char   cTextoExtra1[ kLongTextoExtra + 1 ];
  char   cTextoExtra2[ kLongTextoExtra + 1 ];
  char   cTextoExtra3[ kLongTextoExtra + 1 ];
  char   cFechaExtra[ kLongFecha + 1 ];
  double cImporteExtra1;
  double cImporteExtra2;
  double cImporteExtra3;
  double cImporteExtra4;
} CLIENTEPROVEEDOR;

typedef CLIENTEPROVEEDOR FAR * LPFREGCTEPROV;

// Definici�n de estructura de datos de valor clasificacion ----------------------------------------
typedef struct ValorClasificacion
{
  int  cClasificacionDe;
  int  cNumClasificacion;
  char cCodigoValorClasificacion[ kLongCodValorClasif + 1 ];
  char cValorClasificacion[ kLongDescripcion + 1 ];
} VALORCLASIFICACION;

typedef VALORCLASIFICACION FAR * LPFREGVALORCLASIFICACION;

// Definici�n de estructura de datos de unidad de peso y medida ------------------------------------
typedef struct Unidad
{
  char cNombreUnidad[ kLongNombre + 1 ];
  char cAbreviatura[ kLongAbreviatura + 1 ];
  char cDespliegue[ kLongAbreviatura + 1 ];
} UNIDAD;

typedef UNIDAD FAR * LPFREGUNIDAD;

// Definici�n de estructura de datos de direcciones ------------------------------------------------
typedef struct Direccion {
  char   cCodCteProv[ kLongCodigo + 1 ];
  int    cTipoCatalogo; // 1=Clientes y 2=Proveedores
  int    cTipoDireccion; // 1=Domicilio Fiscal, 2=Domicilio Envio
  char   cNombreCalle[ kLongDescripcion + 1 ];
  char   cNumeroExterior[ kLongNumeroExtInt + 1 ];
  char   cNumeroInterior[ kLongNumeroExtInt + 1 ];
  char   cColonia[ kLongDescripcion + 1 ];
  char   cCodigoPostal[ kLongCodigoPostal + 1 ];
  char   cTelefono1[ kLongTelefono + 1 ];
  char   cTelefono2[ kLongTelefono + 1 ];
  char   cTelefono3[ kLongTelefono + 1 ];
  char   cTelefono4[ kLongTelefono + 1 ];
  char   cEmail[ kLongEmailWeb + 1 ];
  char   cDireccionWeb[ kLongEmailWeb + 1 ];
  char   cCiudad[ kLongDescripcion + 1 ];
  char   cEstado[ kLongDescripcion + 1 ];
  char   cPais[ kLongDescripcion + 1 ];
  char   cTextoExtra[ kLongDescripcion + 1 ];
} DIRECCION;

typedef DIRECCION FAR * LPFREGDIRECCION;

// Definici�n de estructura de datos de Control de IVA  --------------------------------------------
typedef struct ControlIVA {
  double  cNetoTasa15;
  double  cNetoTasa10;
  double  cNetoTasaCero;
  double  cNetoTasaExenta;
  double  cNetoOtrasTasas;
  double  cIVATasa15;
  double  cIVATasa10;
  double  cIVAOtrasTasas;
  double  cRetIVATasa15;
  double  cRetIVATasa10;
  double  cRetIVAOtrasTasas;
  double  cRetISRTasa15;
  double  cRetISRTasa10;
  double  cRetISRTasaCero;
  double  cRetISRTasaExenta;
  double  cRetISROtrasTasas;
  double  cOtrosImpTasa15;
  double  cOtrosImpTasa10;
  double  cOtrosImpTasaCero;
  double  cOtrosImpTasaExenta;
  double  cOtrosImpOtrasTasas;
} CONTROL_IVA;

typedef CONTROL_IVA FAR * LPCONTROL_IVA;

// Nuevos m�todos reforma 2010
// Definici�n de estructura de datos de Control de IVA  --------------------------------------------
typedef struct ControlIVA_2010 {
  double  cNetoTasa15;
  double  cNetoTasa10;
  double  cNetoTasaCero;
  double  cNetoTasaExenta;
  double  cNetoOtrasTasas;
  double  cIVATasa15;
  double  cIVATasa10;
  double  cIVAOtrasTasas;
  double  cRetIVATasa15;
  double  cRetIVATasa10;
  double  cRetIVAOtrasTasas;
  double  cRetISRTasa15;
  double  cRetISRTasa10;
  double  cRetISRTasaCero;
  double  cRetISRTasaExenta;
  double  cRetISROtrasTasas;
  double  cOtrosImpTasa15;
  double  cOtrosImpTasa10;
  double  cOtrosImpTasaCero;
  double  cOtrosImpTasaExenta;
  double  cOtrosImpOtrasTasas;
  double  cNetoTasa16;
  double  cNetoTasa11;
  double  cIVATasa16;
  double  cIVATasa11;
  double  cRetIVATasa16;
  double  cRetIVATasa11;
  double  cRetISRTasa16;
  double  cRetISRTasa11;
  double  cOtrosImpTasa16;
  double  cOtrosImpTasa11;
} CONTROL_IVA_2010;

typedef CONTROL_IVA_2010 FAR * LPCONTROL_IVA_2010;

#pragma pack(pop)

#endif