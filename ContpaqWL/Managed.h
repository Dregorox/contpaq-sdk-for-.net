#pragma once 
#ifndef _MANAGED_H_DRGX_
#define _MANAGED_H_DRGX_
using namespace System;

namespace ContpaqWL{
	public value struct Documento {
		public:
			double	Folio;
			int		NumMoneda;
			double	TipoCambio;
			double	Importe;
			double	DescuentoDoc1;
			double	DescuentoDoc2;
			int		SistemaOrigen;
			String^	CodConcepto;
			String^  Serie;
			String^  Fecha;
			String^  CodigoCteProv;
			String^  CodigoAgente;
			String^	Referencia;
			int		Afecta;
			double	Gasto1;
			double	Gasto2;
			double	Gasto3;
			LPFREGDOCUMENTO ToCStruct();
	};

	public value struct Movimiento {
	public:
		  int		Consecutivo;
		  double	Unidades;
		  double	Precio;
		  double	Costo;
		  String^   CodProdSer;
		  String^   CodAlmacen;
		  String^   Referencia;
		  String^	CodClasificacion;
		  LPFREGMOVIMIENTO ToCStruct();
	};

	public value struct ClienteProveedor{
	public:
		String^		CodigoCliente;
		String^		RazonSocial;
		DateTime^	FechaAlta;
		String^		RFC;
		String^		CURP;
		String^		DenComercial;
		String^		RepLegal;
		String^		NombreMoneda;
		int			ListaPreciosCliente;
		double		DescuentoMovto;
		int			BanVentaCredito; // 0 = No se permite venta a cr�dito, 1 = Se permite venta a cr�dito
		String^		CodigoValorClasificacionCliente1;
		String^		CodigoValorClasificacionCliente2;
		String^		CodigoValorClasificacionCliente3;
		String^		CodigoValorClasificacionCliente4;
		String^		CodigoValorClasificacionCliente5;
		String^		CodigoValorClasificacionCliente6;
		int			TipoCliente; // 1 - Cliente, 2 - Cliente/Proveedor, 3 - Proveedor
		int			Estatus; // 0. Inactivo, 1. Activo
		DateTime^	FechaBaja;
		DateTime^	FechaUltimaRevision;
		double		LimiteCreditoCliente;
		int			DiasCreditoCliente;
		int			BanExcederCredito; // 0 = No se permite exceder cr�dito, 1 = Se permite exceder el cr�dito
		double		DescuentoProntoPago;
		int			DiasProntoPago;
		double		InteresMoratorio;
		int			DiaPago;
		int			DiasRevision;
		String^		Mensajeria;
		String^		CuentaMensajeria;
		int			DiasEmbarqueCliente;
		String^		CodigoAlmacen;
		String^		CodigoAgenteVenta;
		String^		CodigoAgenteCobro;
		int			RestriccionAgente;
		double		Impuesto1;
		double		Impuesto2;
		double		Impuesto3;
		double		RetencionCliente1;
		double		RetencionCliente2;
		String^		CodigoValorClasificacionProveedor1;
		String^		CodigoValorClasificacionProveedor2;
		String^		CodigoValorClasificacionProveedor3;
		String^		CodigoValorClasificacionProveedor4;
		String^		CodigoValorClasificacionProveedor5;
		String^		CodigoValorClasificacionProveedor6;
		double		LimiteCreditoProveedor;
		int			DiasCreditoProveedor;
		int			TiempoEntrega;
		int			DiasEmbarqueProveedor;
		double		ImpuestoProveedor1;
		double		ImpuestoProveedor2;
		double		ImpuestoProveedor3;
		double		RetencionProveedor1;
		double		RetencionProveedor2;
		int			BanInteresMoratorio; // 0 = No se le calculan intereses moratorios al cliente, 1 = Si se le calculan intereses moratorios al cliente.
		String^		TextoExtra1;
		String^		TextoExtra2;
		String^		TextoExtra3;
		DateTime^	FechaExtra;
		double		ImporteExtra1;
		double		ImporteExtra2;
		double		ImporteExtra3;
		double		ImporteExtra4;
		LPFREGCTEPROV ToCStruct();
	};
	
}
#endif