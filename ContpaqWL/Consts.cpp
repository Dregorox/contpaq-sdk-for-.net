#include "Stdafx.h"

using namespace System;

namespace ContpaqWL{

	public value struct FieldsLength{
	public:
		static const int Fecha            =		kLongFecha			;
		static const int Serie            =		kLongSerie         	;
		static const int Codigo           =		kLongCodigo        	;
		static const int Nombre           =		kLongNombre        	;
		static const int Referencia       =		kLongReferencia    	;
		static const int Descripcion      =		kLongDescripcion   	;
		static const int Cuenta           =		kLongCuenta        	;
		static const int Mensaje          =		kLongMensaje       	;
		static const int NombreProducto   =		kLongNombreProducto	;
		static const int Abreviatura      =		kLongAbreviatura   	;
		static const int CodValorClasif   =		kLongCodValorClasif	;
		static const int DenComercial     =		kLongDenComercial  	;
		static const int RepLegal         =		kLongRepLegal      	;
		static const int TextoExtra       =		kLongTextoExtra    	;
		static const int RFC              =		kLongRFC           	;
		static const int CURP             =		kLongCURP          	;
		static const int DesCorta         =		kLongDesCorta      	;
		static const int NumeroExtInt     =		kLongNumeroExtInt  	;
		static const int CodigoPostal     =		kLongCodigoPostal  	;
		static const int Telefono         =		kLongTelefono      	;
		static const int EmailWeb         =		kLongEmailWeb      	;
	};

	public value struct AgentesFields{
	public:
		static const String^ Codigo  = kAgente_Codigo;
		static const String^ Nombre = kAgente_Nombre;
		static const String^ Tipo = kAgente_Tipo;
		static const String^ CodigoCliente = kAgente_CodigoCliente;
		static const String^ CodigoProveedor = kAgente_CodigoProveedor;
		static const String^ CodigoValorClasif1 = kAgente_CodigoValorClasif1;
		static const String^ CodigoValorClasif2 = kAgente_CodigoValorClasif2;
		static const String^ CodigoValorClasif3 = kAgente_CodigoValorClasif3;
		static const String^ CodigoValorClasif4 = kAgente_CodigoValorClasif4;
		static const String^ CodigoValorClasif5 = kAgente_CodigoValorClasif5;
		static const String^ DescProntoPagp = kCteProv_DescProntoPago;
		static const String^ ComisionVenta = kAgente_ComisionVenta;
		static const String^ ComisionCobro = kAgente_ComisionCobro;
		
	};

	public value struct ClientesProveedoresFields{
	public:
		static const String^ Codigo = kCteProv_Codigo;
		static const String^ RazonSocial = kCteProv_RazonSocial;
		static const String^ RFC = kCteProv_RFC;
		static const String^ Tipo = kCteProv_Tipo;
		static const String^ Mensajeria = kCteProv_Mensajeria;
		static const String^ CodigoAgenteVenta = kCteProv_CodigoAgenteVenta;
		static const String^ CodigoAgenteCobro = kCteProv_CodigoAgenteCobro;
		static const String^ CodigoValorClasCte1 = kCteProv_CodigoValorClasCte1;
		static const String^ CodigoValorClasCte2 = kCteProv_CodigoValorClasCte2;
		static const String^ CodigoValorClasCte3 = kCteProv_CodigoValorClasCte3;
		static const String^ CodigoValorClasCte4 = kCteProv_CodigoValorClasCte4;
		static const String^ CodigoValorClasCte5 = kCteProv_CodigoValorClasCte5;
		static const String^ CodigoValorClasCte6 = kCteProv_CodigoValorClasCte6;
		static const String^ CodigoValorClasProv1 = kCteProv_CodigoValorClasProv1;
		static const String^ CodigoValorClasProv2 = kCteProv_CodigoValorClasProv2;
		static const String^ CodigoValorClasProv3 = kCteProv_CodigoValorClasProv3;
		static const String^ CodigoValorClasProv4 = kCteProv_CodigoValorClasProv4;
		static const String^ CodigoValorClasProv5 = kCteProv_CodigoValorClasProv5;
		static const String^ CodigoValorClasProv6 = kCteProv_CodigoValorClasProv6;
		static const String^ DescProntoPago = kCteProv_DescProntoPago;
		static const String^ InteresMoratorio = kCteProv_InteresMoratorio;
		static const String^ NombreMoneda = kCteProv_NombreMoneda;
		static const String^ ComisionVenta = kCteProv_ComisionVenta;
		static const String^ ComisionCobro = kCteProv_ComisionCobro;
		static const String^ CodigoAlmacenConsignacion = kCteProv_CodigoAlmacenConsignacion;
		
	};

	public value struct DireccionesFields{
	public:
		// Campos de la tablas de Direcciones
		static const String^ CodigoCatalogo   = kDireccion_CodigoCatalogo;       
		static const String^ TipoCatalogo     = kDireccion_TipoCatalogo;         
	};
}