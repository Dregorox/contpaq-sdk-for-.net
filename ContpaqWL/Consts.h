
// Campos de la tabla de Agentes
#define kAgente_Codigo                      "cCodigoAgente"
#define kAgente_Nombre                      "cNombreAgente"
#define kAgente_Tipo                        "cTipoAgente"
#define kAgente_CodigoCliente               "cCodigoCliente"
#define kAgente_CodigoProveedor             "cCodigoProveedor"
#define kAgente_CodigoValorClasif1          "cCodigoValorClasif1"
#define kAgente_CodigoValorClasif2          "cCodigoValorClasif2"
#define kAgente_CodigoValorClasif3          "cCodigoValorClasif3"
#define kAgente_CodigoValorClasif4          "cCodigoValorClasif4"
#define kAgente_CodigoValorClasif5          "cCodigoValorClasif5"
#define kAgente_CodigoValorClasif6          "cCodigoValorClasif6"
#define kCteProv_DescProntoPago             "cDescuentoProntoPago"
#define kAgente_ComisionVenta               "cComisionVentaAgente"
#define kAgente_ComisionCobro               "cComisionCobroAgente"

// Campos de la tablas de Clientes/Proveedores
#define kCteProv_Codigo                     "cCodigoCliente"
#define kCteProv_RazonSocial                "cRazonSocial"
#define kCteProv_RFC                        "cRFC"
#define kCteProv_Tipo                       "cTipoCliente"
#define kCteProv_Mensajeria                 "cMensajeria"
#define kCteProv_CodigoAgenteVenta          "cCodigoAgenteVenta"
#define kCteProv_CodigoAgenteCobro          "cCodigoAgenteCobro"
#define kCteProv_CodigoValorClasCte1        "cCodigoValorClasifCte1"
#define kCteProv_CodigoValorClasCte2        "cCodigoValorClasifCte2"
#define kCteProv_CodigoValorClasCte3        "cCodigoValorClasifCte3"
#define kCteProv_CodigoValorClasCte4        "cCodigoValorClasifCte4"
#define kCteProv_CodigoValorClasCte5        "cCodigoValorClasifCte5"
#define kCteProv_CodigoValorClasCte6        "cCodigoValorClasifCte6"
#define kCteProv_CodigoValorClasProv1       "cCodigoValorClasifPro1"
#define kCteProv_CodigoValorClasProv2       "cCodigoValorClasifPro2"
#define kCteProv_CodigoValorClasProv3       "cCodigoValorClasifPro3"
#define kCteProv_CodigoValorClasProv4       "cCodigoValorClasifPro4"
#define kCteProv_CodigoValorClasProv5       "cCodigoValorClasifPro5"
#define kCteProv_CodigoValorClasProv6       "cCodigoValorClasifPro6"
#define kCteProv_DescProntoPago             "cDescuentoProntoPago"
#define kCteProv_InteresMoratorio           "cInteresMoratorio"
#define kCteProv_NombreMoneda               "cNombreMoneda"
#define kCteProv_ComisionVenta              "cComVenta"
#define kCteProv_ComisionCobro              "cComCobro"
#define kCteProv_CodigoAlmacenConsignacion  "cCodigoAlmacen"

// Campos de la tablas de Direcciones
#define kDireccion_CodigoCatalogo           "cCodigoCatalogo"
#define kDireccion_TipoCatalogo             "cTipoCatalogo"

// Campos de la tablas de Almacenes
#define kAlmacen_Codigo                     "cCodigoAlmacen"
#define kAlmacen_CodigoValorClasif1         "cCodigoValorClasif1"
#define kAlmacen_CodigoValorClasif2         "cCodigoValorClasif2"
#define kAlmacen_CodigoValorClasif3         "cCodigoValorClasif3"
#define kAlmacen_CodigoValorClasif4         "cCodigoValorClasif4"
#define kAlmacen_CodigoValorClasif5         "cCodigoValorClasif5"
#define kAlmacen_CodigoValorClasif6         "cCodigoValorClasif6"

// Campos de la tablas de Documentos
#define kDocumento_IdDocumento              "cIdDocumento"
#define kDocumento_CodigoConcepto           "cCodigoConcepto"
#define kDocumento_Serie                    "cSerieDocumento"
#define kDocumento_Folio                    "cFolio"
#define kDocumento_Fecha                    "cFecha"
#define kDocumento_CodigoCteProv            "cCodigoCteProv"
#define kDocumento_RazonSocial              "cRazonSocial"
#define kDocumento_RFC                      "cRFC"
#define kDocumento_SerieOmision             "cSeriePorOmision"
#define kDocumento_CodigoAgente             "cCodigoAgente"
#define kDocumento_FechaVencimiento         "cFechaVencimiento"
#define kDocumento_FechaEntRecep            "cFechaEntregaRecepcion"
#define kDocumento_FechaProntoPago          "cFechaProntoPago"
#define kDocumento_FechaUltimoInteres       "cFechaUltimoInteres"
#define kDocumento_IdMoneda                 "cidMoneda"
#define kDocumento_TipoCambio               "cTipoCambio"
#define kDocumento_Referencia               "cReferencia"
#define kDocumento_Importe                  "cImporte"
#define kDocumento_Descuento1               "cDescuentoDoc1"
#define kDocumento_Descuento2               "cDescuentoDoc2"
#define kDocumento_DescProntoPago           "cDescuentoProntoPago"
#define kDocumento_InteresMoratorio         "cPorcentajeInteres"
#define kDocumento_SisOrigen                "cSistOrig"
#define kDocumento_Observaciones            "cObservaciones"
#define kDocumento_ConDireccionFiscal       "cConDireccionFiscal"
#define kDocumento_ConDireccionEnvio        "cConDireccionEnvio"
#define kDocumento_Gasto1                   "cGasto1"
#define kDocumento_Gasto2                   "cGasto2"
#define kDocumento_Gasto3                   "cGasto3"

// Campos de la tablas de Movimientos
#define kMovto_IdMovto                      "cIdMovimiento"
#define kMovto_NumMovto                     "cNumeroMovimiento"
#define kMovto_CodProducto                  "cCodigoProducto"
#define kMovto_CodAlmacen                   "cCodigoAlmacen"
#define kMovto_CodAlmacenEntrada            "cCodigoAlmacenEntrada"
#define kMovto_CodAlmacenSalida             "cCodigoAlmacenSalida"
#define kMovto_CodAlmacenConsignacion       "cCodigoAlmacenConsignacion"
#define kMovto_Unidades                     "cUnidades"
#define kMovto_UnidadesNC                   "cUnidadesNC"
#define kMovto_Precio                       "cPrecio"
#define kMovto_CostoCapturado               "cCostoCapturado"
#define kMovto_CodValorClasificacion        "cCodigoValorClasificacion"
#define kMovto_Referencia                   "cReferencia"
#define kMovto_FechaExtra                   "cFechaExtra"
#define kMovto_NombreUnidad                 "cNombreUnidad"
#define kMovto_NombreUnidadNC               "cNombreUnidadNC"
#define kMovto_CodClasificacion             "cCodigoClasificacion"
#define kMovto_Neto                         "cNeto"
#define kMovto_Total                        "cTotal"
#define kMovto_ObservaMov                   "cObservaMov"
#define kMovto_ComisionVenta	              "cComVenta"

#define kMovto_PorcDescto1                  "cPorcentajeDescuento1"
#define kMovto_ImportDescto1                "cDescuento1"
#define kMovto_PorcDescto2                  "cPorcentajeDescuento2"
#define kMovto_ImportDescto2                "cDescuento2"
#define kMovto_PorcDescto3                  "cPorcentajeDescuento3"
#define kMovto_ImportDescto3                "cDescuento3"
#define kMovto_PorcDescto4                  "cPorcentajeDescuento4"
#define kMovto_ImportDescto4                "cDescuento4"
#define kMovto_PorcDescto5                  "cPorcentajeDescuento5"
#define kMovto_ImportDescto5                "cDescuento5"

// Campos de la tabla de Productos
#define kProducto_Codigo                    "cCodigoProducto"
#define kProducto_Nombre                    "cNombreProducto"
#define kProducto_Tipo                      "cTipoProducto"
#define kProducto_Precio1                   "cPrecio1"
#define kProducto_NombrePadreCarac1         "cNombrePadreCarac1"
#define kProducto_NombrePadreCarac2         "cNombrePadreCarac2"
#define kProducto_NombrePadreCarac3         "cNombrePadreCarac3"
#define kProducto_CodigoValorClasif1        "cCodigoValorClasif1"
#define kProducto_CodigoValorClasif2        "cCodigoValorClasif2"
#define kProducto_CodigoValorClasif3        "cCodigoValorClasif3"
#define kProducto_CodigoValorClasif4        "cCodigoValorClasif4"
#define kProducto_CodigoValorClasif5        "cCodigoValorClasif5"
#define kProducto_CodigoValorClasif6        "cCodigoValorClasif6"
#define kProducto_NombreUnidadBase          "cNombreUnidadBase"
#define kProducto_NombreUnidadCompra        "cNombreUnidadCompra"
#define kProducto_NombreUnidadVenta         "cNombreUnidadVenta"
#define kProducto_NombreUnidadNoConvertible "cNombreUnidadNoConvertible"
#define kProducto_Descripcion               "cDescripcionProducto"
#define kProducto_MetodoCosteo              "cMetodoCosteo"
#define kProducto_ComisionVenta	            "cComVentaExcepProducto"
#define kProducto_MonedaCostoExtra1         "cMonedaCostoExtra1"
#define kProducto_MonedaCostoExtra2         "cMonedaCostoExtra2"
#define kProducto_MonedaCostoExtra3         "cMonedaCostoExtra3"
#define kProducto_MonedaCostoExtra4         "cMonedaCostoExtra4"
#define kProducto_MonedaCostoExtra5         "cMonedaCostoExtra5"
#define kProducto_MargenUtilidad            "cMargenUtilidad"
#define kProducto_ComisionCobro	            "cComCobroExcepProducto"


// Campos de la tabla de Promociones
#define kPromocion_cCodigoValorClasifCliente1   "cCodigoValorClasifCliente1"
#define kPromocion_cCodigoValorClasifCliente2   "cCodigoValorClasifCliente2"
#define kPromocion_cCodigoValorClasifCliente3   "cCodigoValorClasifCliente3"
#define kPromocion_cCodigoValorClasifCliente4   "cCodigoValorClasifCliente4"
#define kPromocion_cCodigoValorClasifCliente5   "cCodigoValorClasifCliente5"
#define kPromocion_cCodigoValorClasifCliente6   "cCodigoValorClasifCliente6"
#define kPromocion_cCodigoValorClasifProducto1  "cCodigoValorClasifProducto1"
#define kPromocion_cCodigoValorClasifProducto2  "cCodigoValorClasifProducto2"
#define kPromocion_cCodigoValorClasifProducto3  "cCodigoValorClasifProducto3"
#define kPromocion_cCodigoValorClasifProducto4  "cCodigoValorClasifProducto4"
#define kPromocion_cCodigoValorClasifProducto5  "cCodigoValorClasifProducto5"
#define kPromocion_cCodigoValorClasifProducto6  "cCodigoValorClasifProducto6"

#define kDECIMALES_PORCENTAJES    2
// Campos de la tabla de Series
#define kSerie_NumeroSerie                  "cNumeroSerie"

// Campos de la tabla de Capas
#define kCapas_Pedimento                    "cPedimento"
#define kCapas_NumeroLote                   "cNumeroLote"
#define kCapas_Aduana                       "cAduana"
#define kCapas_FechaPedimento               "cFechaPedimento"
#define kCapas_FechaFabricacion             "cFechaFabricacion"
#define kCapas_FechaCaducidad               "cFechaCaducidad"
#define kCapas_Existencia                   "cExistencia"
#define kCapas_TipoCapa                     "cTipoCapa"
#define kCapas_Fecha                        "cFecha"
#define kCapas_Costo                        "cCosto"
#define kCapas_TipoCambio                   "cTipoCambio"

// Campos de la tabla de Unidades
#define kUnidades_IdUnidad                  "cIdUnidad"
#define kUnidades_NombreUnidad              "cNombreUnidad"
#define kUnidades_Abreviatura               "cAbreviatura"

// Campos de la tabla de Cajas
#define kCaja_Codigo                        "cCodigoC01"
#define kClave_Usuario                      "Clave"

// Definicion de constantes de longitudes de campos
#define kLongFecha                23
#define kLongSerie                11
#define kLongCodigo               30
#define kLongNombre               60
#define kLongReferencia           20
#define kLongDescripcion          60
#define kLongCuenta              100
#define kLongMensaje             255
#define kLongNombreProducto      255
#define kLongAbreviatura           3
#define kLongCodValorClasif        3
#define kLongDenComercial         50
#define kLongRepLegal             50
#define kLongTextoExtra           50
#define kLongRFC                  20
#define kLongCURP                 20
#define kLongDesCorta             20
#define kLongNumeroExtInt          6
#define kLongCodigoPostal          6
#define kLongTelefono             15
#define kLongEmailWeb             50

// Constantes para el m�todo fLlenaRegistro...
#define kAlta_Registro              1
#define kActualizacion_Registro     2
