#pragma once
#ifndef _CONTPAQWL_H_DRGX_
#define _CONTPAQWL_H_DRGX_

// ContpaqWL.h
//#include "MGW_SDK.h"
#include "Managed.h"

/**Definiciones locales**/
//Maximo tama�o de un campo de txto.
#define kMaxFieldLen	kLongMensaje
#define MAXSTRINGLEN	1024

/* Definicion de tipos para funciones con parametros y retornos muy utilizados */
typedef int (CALLBACK* LPFNDLL_IV)(void);
typedef int (CALLBACK* LPFNDLL_II)(int);
typedef void (CALLBACK* LPFNDLL_VV)(void);
typedef void (CALLBACK* LPFNDLL_VICI) (int, char *, int);
typedef int (CALLBACK* LPFNDLL_IICC) (int&, char *, char*); 
typedef int (CALLBACK* LPFNDLL_IC) (char *);

/* Definicion de tipos para funciones con parametros y retornos poco utilizados */
/* Documentos/Movimientos */
typedef int (CALLBACK* LPFNDLL_ALTADOCUMENTO) (long&, LPFREGDOCUMENTO);
typedef int (CALLBACK* LPFNDLL_ALTAMOVIMIENTO) (long , long&, LPFREGMOVIMIENTO );
/* Clientes/Proveedores */
typedef int (CALLBACK* LPFNDLL_BUSCAIDCTEPROV) (int);
typedef int (CALLBACK* LPFNDLL_LEEDATOCTEPROV) (const char*, char*, int);
typedef int (CALLBACK* LPFNDLL_ALTACTEPROV) (int&, LPFREGCTEPROV);
typedef int (CALLBACK* LPFNDLL_ACTUALIZACTEPROV) (char*, LPFREGCTEPROV);
using namespace System;

namespace ContpaqWL {
	

	public ref class Contpaq
	{
	private:
		HMODULE hDll;
		int inicializado;
		/* Apuntadores a funciones generales */ 
		LPFNDLL_IV fInicializaSDK;
		LPFNDLL_VICI fError;
		LPFNDLL_VV fTerminaSDK;
		LPFNDLL_IICC fPosPrimerEmpresa;
		LPFNDLL_IICC fPosSiguienteEmpresa;
		LPFNDLL_VV fCierraEmpresa;
		LPFNDLL_IC fAbreEmpresa;
		// Documentos/Movimientos
		LPFNDLL_ALTADOCUMENTO fAltaDocumento;
		LPFNDLL_ALTAMOVIMIENTO fAltaMovimiento;
		// Clientes/Proveedores
		LPFNDLL_II fBuscaIdCteProv;
		LPFNDLL_IC fBuscaCteProv;

		LPFNDLL_LEEDATOCTEPROV fLeeDatoCteProv;
		LPFNDLL_ALTACTEPROV	fAltaCteProv;
		LPFNDLL_ACTUALIZACTEPROV fActualizaCteProv;

		LPFNDLL_IV fPosPrimerCteProv;
		LPFNDLL_IV fPosUltimoCteProv;
		LPFNDLL_IV fPosSiguienteCteProv;
		LPFNDLL_IV fPosAnteriorCteProv;
		LPFNDLL_IV fPosBOFCteProv;
		LPFNDLL_IV fPosEOFCteProv;
	public:
		//Constructor 
		Contpaq();
		// int		SetNombrePAQ ( String^ aSistema );
#pragma region General
		void	InicializaSDK		( void );
		void	TerminaSDK			( void );
		String^	DescripcionError	( int aNumError);
		//int		InicializaLicenseInfo(unsigned char aSistema);
		//void	SetModoImportacion( bool aImportacion );
#pragma endregion		

#pragma region Navegacion de empresas
		int		PosPrimerEmpresa		( int% IdEmpresa, String^% NombreEmpresa, String^% DirectorioEmpresa );
		int		PosSiguienteEmpresa		( int% IdEmpresa, String^% NombreEmpresa, String^% DirectorioEmpresa );
		int		AbreEmpresa				( String^ aDirectorioEmpresa );
		void	CierraEmpresa			( void );
#pragma endregion

#pragma region Documentos/Movimientos - Alto Nivel 
		//Altas Bajas documentos - Alto Nivel
		int	AltaDocumento	( long% IdDocumento, ContpaqWL::Documento Documento);
		int AltaMovimiento (long IdDocumento, long% IdMovimiento, ContpaqWL::Movimiento Movimiento);
#pragma endregion

#pragma region Clientes/Proveedores - Alto Nivel
		int BuscaIdCteProv (int IdCteProv);
		int BuscaCteProv (String^ CodCteProv);
		int LeeDatoCteProv (String^ Campo, String^% Valor);
		int AltaCteProv (int% IdCteProv, ClienteProveedor CteProv);
		int ActualizaCteProv (String^ CodigoCteProv, ClienteProveedor CteProv);
		int PosPrimerCteProv();
		int PosUltimoCteProv();
		int PosSiguienteCteProv();
		int PosAnteriorCteProv();
		int PosBOFCteProv();
		int PosEOFCteProv();
		
#pragma endregion

	};

	public ref class ContpaqException : System::ApplicationException {
	public:
		ContpaqException (String^ Message, int ErrorNumber);
	};
}
#endif