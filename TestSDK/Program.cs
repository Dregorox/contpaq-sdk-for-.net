﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContpaqSDK;
using ContpaqWL;


namespace TestSDK
{
    class Program
    {
        static void Main(string[] args) {
            /* NOTA: Este codigo no implementa muchas validaciones, 
             * asi que usar con los datos que se esperan o una excepcion 
             * podria ser lanzada y golpearte en la cara :D 
             */
            ContpaqNET sdk = null;
            try {
                sdk = new ContpaqNET();
                List<Empresa> empresas = sdk.EnlistarEmpresas();
                int c = 0;
                foreach (Empresa  empresa in sdk.EnlistarEmpresas()) {
                    Console.WriteLine(String.Format("{3}.- Nombre de la empresa: [{1}] {0}, Directorio: {2}", new object[] {empresa.Nombre, empresa.IdEmpresa, empresa.Directorio, c++}));
                    
                }
                int opcion;
                Console.WriteLine("Selecciona la empresa que deseas abrir: ");
                opcion = int.Parse(Console.ReadLine());

                /* Abrimos la empresa seleccionada */
                sdk.AbreEmpresa(empresas[opcion]);

                /* Alta de cliente */
                sdk.AltaCteProv(CrearCliente());

                /* Cerramos la empresa */
                sdk.CierraEmpresa();
            }
            catch (System.Runtime.InteropServices.SEHException seh) {
                Type t = seh.GetType();
                Console.WriteLine(string.Format("{0}[{3}]: {1} - {2}", new object[] { t.FullName, seh.Message, seh.StackTrace , seh.ErrorCode}));
            }
            catch (Exception ex) {
                Type t = ex.GetType();
                Console.WriteLine(string.Format("{0}: {1} - {2}", new object[] { t.FullName, ex.Message, ex.StackTrace}));
                
            }
            Console.ReadKey();
            if (sdk != null) sdk.Dispose();
        }

        private static ClienteProveedor CrearCliente()
        {
            ClienteProveedor cp = new ClienteProveedor();

            string CodigoCliente = "";
            string RazonSocial = "";

            cp.CodigoCliente = "1234";
            cp.RazonSocial = "Los Ejemplos S.A.";
            cp.RFC = "LES790614T3Q";
            cp.TipoCliente = 1;
            cp.Estatus = 1; //Estatus 1 es activo
            cp.NombreMoneda = "Peso Mexicano";


            return cp;
        }
    }
}
